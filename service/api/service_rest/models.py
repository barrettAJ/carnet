from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse(
                'api_detail_technician',
                kwargs={'pk': self.id}
            )


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    is_vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name='technician',
        on_delete=models.CASCADE,
    )
    status = models.CharField(max_length=20, default="CREATED")

    def cancel(self):
        status = "CANCELED"
        self.status = status
        self.save()

    def finish(self):
        status = "FINISHED"
        self.status = status
        self.save()

    def get_api_url(self):
        return reverse('api_show_appointment', kwargs={'id': self.id})

    def __str__(self):
        return self.vin

    class Meta():
        ordering = ['date_time']

    def save(self, *args, **kwargs):
        if AutomobileVO.objects.filter(vin=self.vin).exists():
            self.is_vip = True
        super().save(*args, **kwargs)
